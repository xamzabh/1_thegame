﻿namespace TheGame
{
    public class Weapon
    {
        public Weapon(bool isActive, Coord position)
        {
            this.IsActive = isActive;
            this.Position = position;
        }

        public Coord Position { get; private set; }

        public bool IsActive { get; private set; }

        public void Activate()
        {
            this.IsActive = true;
        }

        public void Deactivate()
        {
            this.IsActive = false;
        }
    }
}
