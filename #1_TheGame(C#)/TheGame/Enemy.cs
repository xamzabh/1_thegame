﻿namespace TheGame
{
    using System;
    using System.Collections.Generic;

    public enum EnemySize
    {
        Small = 1,
        Medium = 2,
        Large = 3
    }

    public class Enemy
    {
        private ConsoleColor color;
        private double fireSpeed = 0.1;
        private long lastFire = 0;
        private long lastMove = 0;
        private int maxHealth;
        private double speed = 2;

        public Enemy(Coord position, EnemySize size)
        {
            this.Position = position;
            this.Size = size;
            this.lastFire = Game.Timer.ElapsedMilliseconds;

            switch (this.Size)
            {
            case EnemySize.Small:
                this.Health = 50;
                this.maxHealth = 50;
                break;
            case EnemySize.Medium:
                this.Health = 100;
                this.maxHealth = 100;
                break;
            case EnemySize.Large:
                this.Health = 150;
                this.maxHealth = 150;
                break;
            }
        }

        public int Health { get; private set; }

        public Coord Position { get; private set; }

        public EnemySize Size { get; private set; }

        public void Clear()
        {
            this.Health = 0;
        }

        public void Fire(List<Bullet> bullets)
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastFire + (1000 / this.fireSpeed))
            {
                Coord position = new Coord(this.Position.X, this.Position.Y);

                switch (this.Size)
                {
                case EnemySize.Small:
                    position += new Coord(1, 1);
                    break;
                case EnemySize.Medium:
                    position += new Coord(2, 2);
                    break;
                case EnemySize.Large:
                    position += new Coord(3, 3);
                    break;
                }

                bullets.Add(new Bullet(position, Direction.Down, false, 3));

                this.lastFire = Game.Timer.ElapsedMilliseconds;
            }
        }

        public bool HasCollision(Bullet bullet)
        {
            if (this.Health > 0 && bullet.Direction == Direction.Up)
            {
                switch (this.Size)
                {
                    case EnemySize.Small:
                        if (this.Position.X <= bullet.Position.X && this.Position.X + 1 >= bullet.Position.X)
                        {
                            if (this.Position.Y <= bullet.Position.Y && this.Position.Y + 1 >= bullet.Position.Y)
                            {
                                return true;
                            }
                        }

                        return false;
                    case EnemySize.Medium:
                        if (this.Position.X <= bullet.Position.X && this.Position.X + 3 >= bullet.Position.X)
                        {
                            if (this.Position.Y <= bullet.Position.Y && this.Position.Y + 2 >= bullet.Position.Y)
                            {
                                return true;
                            }
                        }

                        return false;
                    case EnemySize.Large:
                        if (this.Position.X <= bullet.Position.X && this.Position.X + 5 >= bullet.Position.X)
                        {
                            if (this.Position.Y <= bullet.Position.Y && this.Position.Y + 3 >= bullet.Position.Y)
                            {
                                return true;
                            }
                        }

                        return false;
                    default:
                        return false;
                }
            }

            return false;
        }

        public bool HasCollision(Ship playerShip)
        {
            // If enemy is not dead
            if (this.Health > 0)
            {
                switch (this.Size)
                {
                    case EnemySize.Small:
                        if (this.Position.X + 1 >= playerShip.Position.X && this.Position.X <= playerShip.Position.X + Ship.Width && this.Position.Y >= Console.WindowHeight - 1 - 2 - 1)
                        {
                            return true;
                        }

                        return false;
                    case EnemySize.Medium:
                        if (this.Position.X + 3 >= playerShip.Position.X && this.Position.X <= playerShip.Position.X + Ship.Width && this.Position.Y >= Console.WindowHeight - 1 - 2 - 2)
                        {
                            return true;
                        }

                        return false;
                    case EnemySize.Large:
                        if (this.Position.X + 5 >= playerShip.Position.X && this.Position.X <= playerShip.Position.X + Ship.Width && this.Position.Y >= Console.WindowHeight - 1 - 2 - 3)
                        {
                            return true;
                        }

                        return false;
                    default:
                        return false;
                }
            }

            return false;
        }

        public int Hit(Bullet bullet)
        {
            int score;

            if (this.Health < bullet.Damage || bullet.IsGolden)
            {
                score = this.Health;
                this.Health = 0;
            }
            else
            {
                score = bullet.Damage;
                this.Health -= bullet.Damage;
            }

            bullet.Clear();

            return score;
        }

        public void Hit(Ship ship)
        {
            this.Clear();
        }

        public void Move()
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastMove + (1000 / this.speed))
            {
                this.Position += new Coord(0, 1);

                this.lastMove = Game.Timer.ElapsedMilliseconds;
            }
        }

        public void Print()
        {
            double healthPercent = Convert.ToDouble(this.Health) / this.maxHealth;

            if (healthPercent <= 0.20)
            {
                this.color = ConsoleColor.Red;
            }
            else if (healthPercent <= 0.60)
            {
                this.color = ConsoleColor.Yellow;
            }
            else
            {
                this.color = ConsoleColor.Green;
            }

            if (this.Size == EnemySize.Small)
            {
                if (this.Position.Y < Console.WindowHeight)
                {
                    ScreenBuffer.Write("__", this.Position.X, this.Position.Y, this.color);
                }

                if (this.Position.Y + 1 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("\\/", this.Position.X, this.Position.Y + 1, this.color);
                }
            }
            else if (this.Size == EnemySize.Medium)
            {
                if (this.Position.Y < Console.WindowHeight)
                {
                    ScreenBuffer.Write("____", this.Position.X, this.Position.Y, this.color);
                }

                if (this.Position.Y + 1 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("\\vv/", this.Position.X, this.Position.Y + 1, this.color);
                }

                if (this.Position.Y + 2 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("VV", this.Position.X + 1, this.Position.Y + 2, this.color);
                }
            }
            else if (this.Size == EnemySize.Large)
            {
                if (this.Position.Y < Console.WindowHeight)
                {
                    ScreenBuffer.Write("______", this.Position.X, this.Position.Y, this.color);
                }

                if (this.Position.Y + 1 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("\\VvvV/", this.Position.X, this.Position.Y + 1, this.color);
                }

                if (this.Position.Y + 2 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("\\VV/", this.Position.X + 1, this.Position.Y + 2, this.color);
                }

                if (this.Position.Y + 3 < Console.WindowHeight)
                {
                    ScreenBuffer.Write("'\\/'", this.Position.X + 1, this.Position.Y + 3, this.color);
                }
            }
        }
    }
}