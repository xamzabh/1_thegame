﻿namespace TheGame
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Ship
    {
        private List<Effect> effects = new List<Effect>();
        private double fireSpeed = 4;
        private bool isGolden = false;
        private long lastFire = 0;
        private long lastMove = 0;
        private double speed = 50;
        private List<Weapon> weapons = new List<Weapon>();

        static Ship()
        {
            Width = 9;
            Height = 3;
        }

        public Ship(Coord position)
        {
            this.Health = 500;
            this.Position = position;
            this.weapons.Add(new Weapon(true, new Coord(4, -1)));
            this.weapons.Add(new Weapon(false, new Coord(1, 0)));
            this.weapons.Add(new Weapon(false, new Coord(7, 0)));
        }

        public static int Width { get; private set; }

        public static int Height { get; private set; }

        public Coord Position { get; private set; }

        public bool IsInvulnerable { get; private set; }

        public int Health { get; private set; }

        public void ActivateWeapon()
        {
            for (int i = 1; i < this.weapons.Count; i++)
            {
                if (this.weapons[i].IsActive == false)
                {
                    this.weapons[i].Activate();
                    break;
                }
            }
        }

        public int ActiveWeaponsCount()
        {
            return this.weapons.Count(weapon => weapon.IsActive == true);
        }

        public void AddEffect(Effect effect)
        {
            if (effect.Type == EffectType.GoldenBullets)
            {
                effect.Duration = Game.Timer.ElapsedMilliseconds + (1 * 1000);
                this.isGolden = true;
            }
            else if (effect.Type == EffectType.Invulnerability)
            {
                effect.Duration = Game.Timer.ElapsedMilliseconds + (1 * 1000);
                this.IsInvulnerable = true;
            }
            else if (effect.Type == EffectType.AddWeapon)
            {
                this.ActivateWeapon();
                return;
            }

            this.effects.Add(effect);
        }

        public void DeactivateWeapon()
        {
            for (int i = this.weapons.Count - 1; i > 0; i--)
            {
                if (this.weapons[i].IsActive == true)
                {
                    this.weapons[i].Deactivate();
                    break;
                }
            }
        }

        public void DecreaseSpeed()
        {
            if (this.speed > 1)
            {
                this.speed--;
            }
        }

        public void Fire(List<Bullet> bullets)
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastFire + (1000 / this.fireSpeed))
            {
                foreach (Weapon weapon in this.weapons)
                {
                    if (weapon.IsActive)
                    {
                        bullets.Add(new Bullet(this.Position + weapon.Position, Direction.Up, this.isGolden));
                    }
                }

                this.lastFire = Game.Timer.ElapsedMilliseconds;
            }
        }

        public bool HasCollision(Bullet bullet)
        {
            if (bullet.Direction == Direction.Down && this.Position.X <= bullet.Position.X && this.Position.X + Ship.Width >= bullet.Position.X && bullet.Position.Y >= Game.Height - 1 - 3)
            {
                return true;
            }

            return false;
        }

        public void Hit(Bullet bullet)
        {
            if (!this.IsInvulnerable)
            {
                this.Health -= bullet.Damage;
            }

            bullet.Clear();
        }

        public void Hit(Enemy enemy)
        {
            if (!this.IsInvulnerable)
            {
                switch (enemy.Size)
                {
                    case EnemySize.Small:
                        this.Health -= 20;
                        break;
                    case EnemySize.Medium:
                        this.Health -= 50;
                        break;
                    case EnemySize.Large:
                        this.Health -= 100;
                        break;
                }
            }
        }

        public void IncreaseSpeed()
        {
            if (this.speed < 4)
            {
                this.speed++;
            }
        }

        public void MoveLeft(int limit)
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastMove + (1000 / this.speed))
            {
                if (this.Position.X > limit)
                {
                    this.Position += new Coord(-1, 0);
                    this.lastMove = Game.Timer.ElapsedMilliseconds;
                }
            }
        }

        public void MoveRight(int limit)
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastMove + (1000 / this.speed))
            {
                if (this.Position.X < limit - 1 - Ship.Width - 1)
                {
                    this.Position += new Coord(1, 0);
                    this.lastMove = Game.Timer.ElapsedMilliseconds;
                }
            }
        }

        public void Print()
        {
            if (this.IsInvulnerable)
            {
                ScreenBuffer.Write("._/^\\_.", this.Position.X + 1, this.Position.Y + 1, ConsoleColor.Magenta);
                ScreenBuffer.Write("<U>\\U/<U>", this.Position.X, this.Position.Y + 2, ConsoleColor.Magenta);
                ScreenBuffer.Write("/0\\", this.Position.X + 3, this.Position.Y + 3, ConsoleColor.Magenta);
            }
            else
            {
                ScreenBuffer.Write("._/^\\_.", this.Position.X + 1, this.Position.Y + 1, ConsoleColor.White);
                ScreenBuffer.Write("<U>\\U/<U>", this.Position.X, this.Position.Y + 2, ConsoleColor.Green);
                ScreenBuffer.Write("/0\\", this.Position.X + 3, this.Position.Y + 3, ConsoleColor.Red);
            }
        }

        public void RemoveEffect(Effect effect)
        {
            if (effect.Type == EffectType.GoldenBullets)
            {
                if (1 == this.effects.Count(e => e.Type == EffectType.GoldenBullets))
                {
                    this.isGolden = false;
                }
            }
            else if (effect.Type == EffectType.Invulnerability)
            {
                if (1 == this.effects.Count(e => e.Type == EffectType.Invulnerability))
                {
                    this.IsInvulnerable = false;
                }
            }

            this.effects.Remove(effect);
        }

        public IEnumerable<Effect> ElapsedEffects(long elapsedMiliseconds)
        {
            return this.effects.FindAll(effect => effect.IsElapsed(elapsedMiliseconds));
        }
    }
}