﻿namespace TheGame
{
    using System;

    public enum EffectType
    {
        GoldenBullets,
        Invulnerability,
        AddWeapon,
    }

    public class Effect
    {
        private long lastMove;
        private double speed = 12;

        public Effect(Coord position, EffectType type)
        {
            this.Position = position;
            this.Type = type;
            this.Duration = 0;
        }

        public Coord Position { get; private set; }

        public EffectType Type { get; private set; }

        public long Duration { get; set; }

        public void Clear()
        {
            this.Position = new Coord(0, Game.Height + 1);
        }

        public bool HasCollision(Ship playerShip)
        {
            if (this.Position.X >= playerShip.Position.X && this.Position.X <= playerShip.Position.X + Ship.Width && this.Position.Y >= Console.WindowHeight - 1 - 3)
            {
                return true;
            }

            return false;
        }

        public void Move()
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastMove + (1000 / this.speed))
            {
                this.Position += new Coord(0, 1);

                this.lastMove = Game.Timer.ElapsedMilliseconds;
            }
        }

        public void Print()
        {
            if (this.Position.Y < Game.Height)
            {
                switch (this.Type)
                {
                    case EffectType.AddWeapon:
                        ScreenBuffer.Write("<W+>", this.Position.X, this.Position.Y, ConsoleColor.Cyan);
                        break;
                    case EffectType.GoldenBullets:
                        ScreenBuffer.Write("<GB>", this.Position.X, this.Position.Y, ConsoleColor.Yellow);
                        break;
                    case EffectType.Invulnerability:
                        ScreenBuffer.Write("<II>", this.Position.X, this.Position.Y, ConsoleColor.Red);
                        break;
                }
            }
        }

        public bool IsElapsed(long elapsedMiliseconds)
        {
            return this.Duration < elapsedMiliseconds;
        }

        public Effect Clone()
        {
            return new Effect(this.Position, this.Type);
        }
    }
}