﻿namespace TheGame
{
    using System;

    public enum Direction
    {
        Up = 1,
        Down = 2
    }

    public class Bullet
    {
        private ConsoleColor color = ConsoleColor.White;
        private long lastMove = 0;
        private double speed = 12;

        public Bullet(Coord position, Direction direction, bool isGolden, double speed = 12)
        {
            this.Position = position;
            this.Direction = direction;
            this.IsGolden = isGolden;
            this.speed = speed;
            this.Damage = 20;
        }

        public bool IsGolden { get; private set; }

        public int Damage { get; private set; }

        public Coord Position { get; private set; }

        public Direction Direction { get; private set; }

        public void Clear()
        {
            switch (this.Direction)
            {
                case Direction.Up:
                    this.Position = new Coord(0, -1);
                    break;
                case Direction.Down:
                    this.Position = new Coord(0, Console.WindowHeight);
                    break;
            }
        }

        public void Move()
        {
            if (Game.Timer.ElapsedMilliseconds >= this.lastMove + (1000 / this.speed))
            {
                if (this.Direction == Direction.Up)
                {
                    this.Position += new Coord(0, -1);
                }
                else if (this.Direction == Direction.Down)
                {
                    this.Position += new Coord(0, 1);
                }

                this.lastMove = Game.Timer.ElapsedMilliseconds;
            }
        }

        public void Print()
        {
            if (this.Direction == Direction.Up)
            {
                if (this.Position.Y < 0)
                {
                    return;
                }

                this.color = ConsoleColor.Cyan;
            }
            else if (this.Direction == Direction.Down)
            {
                if (this.Position.Y > Console.WindowHeight - 1)
                {
                    return;
                }

                this.color = ConsoleColor.Blue;
            }

            if (this.IsGolden)
            {
                this.color = ConsoleColor.Yellow;
            }

            ScreenBuffer.Write("*", this.Position.X, this.Position.Y, this.color);
        }
    }
}