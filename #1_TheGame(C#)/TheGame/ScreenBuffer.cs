﻿namespace TheGame
{
    using Microsoft.Win32.SafeHandles;
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    public class ScreenBuffer
    {
        private static CharInfo[] buffer = new CharInfo[Game.Width * Game.Height];
        private static SafeFileHandle fileHandle = CreateFile("CONOUT$", 0x40000000, 2, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);
        private static SmallRect rectangle = new SmallRect() 
        {
            Left = 0, Top = 0, Right = Game.Width, Bottom = Game.Height
        };

        [STAThread]
        public static void Display()
        {
            if (!fileHandle.IsInvalid)
            {
                WriteConsoleOutput(
                    fileHandle,
                    buffer,
                    new Coord()
                    {
                        X = Game.Width, Y = Game.Height
                    },
                    new Coord()
                    {
                        X = 0, Y = 0
                    },
                    ref rectangle);

                for (int i = 0; i < buffer.Length; i++)
                {
                    buffer[i].Char.AsciiChar = 32;
                }
            }
        }

        public static void Write(string characters, int left, int top, ConsoleColor color)
        {
            byte[] byteArray = Encoding.ASCII.GetBytes(characters);

            for (int i = 0; i < byteArray.Length; i++)
            {
                buffer[(Game.Width * top) + left + i].Attributes = (short)color;
                buffer[(Game.Width * top) + left + i].Char.AsciiChar = byteArray[i];
            }
        }

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern SafeFileHandle CreateFile(
            string fileName,
            [MarshalAs(UnmanagedType.U4)] uint fileAccess,
            [MarshalAs(UnmanagedType.U4)] uint fileShare,
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] int flags,
            IntPtr template);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteConsoleOutput(
            SafeFileHandle consoleOutput,
            CharInfo[] buffer,
            Coord bufferSize,
            Coord bufferCoord,
            ref SmallRect writeRegion);

        [StructLayout(LayoutKind.Explicit)]
        public struct CharInfo
        {
            [FieldOffset(0)]
            public CharUnion Char;
            [FieldOffset(2)]
            public short Attributes;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct CharUnion
        {
            [FieldOffset(0)]
            public char UnicodeChar;
            [FieldOffset(0)]
            public byte AsciiChar;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Coord
        {
            public short X;
            public short Y;

            public Coord(short x, short y)
            {
                this.X = x;
                this.Y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SmallRect
        {
            public short Left;
            public short Top;
            public short Right;
            public short Bottom;
        }
    }
}