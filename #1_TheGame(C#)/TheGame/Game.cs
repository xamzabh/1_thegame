﻿namespace TheGame
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;

    public class Game
    {
        private static short playfield = 100;

        private int counter = 0;
        private int score = 0;
        private List<Bullet> bullets = new List<Bullet>();
        private List<Effect> effects = new List<Effect>();
        private double effectSpawnSpeed = 0.1;
        private Array effectTypes = Enum.GetValues(typeof(EffectType));
        private List<Enemy> enemies = new List<Enemy>();
        private Array enemySizes = Enum.GetValues(typeof(EnemySize));
        private double enemySpawnSpeed = 0.4; // Enemies Per Second
        private long lastEffectSpawn = 0;
        private long lastEnemySpawn = 0;
        private Ship ship = new Ship(new Coord((playfield / 2) - (Ship.Width / 2), Game.Height - Ship.Height - 1));

        static Game()
        {
            Game.Random = new Random();
            Game.Timer = new Stopwatch();
            Game.Width = 120;
            Game.Height = 50;
            Game.Timer.Start();
        }

        public Game()
        {
            // Window initialization
            Console.CursorVisible = false;
            Console.BufferHeight = Console.WindowHeight = Game.Height;
            Console.BufferWidth = Console.WindowWidth = Game.Width;
        }

        public static Random Random { get; private set; }

        public static Stopwatch Timer { get; private set; }

        public static short Height { get; private set; }

        public static short Width { get; private set; }

        public int Score { get; private set; }

        public void Run()
        {
            // Game loop
            while (Game.Timer.IsRunning)
            {
                this.GenerateEnemies();
                this.GenerateEffects();

                this.GetUserInput();

                this.GameLogic();

                this.PrintObjects();

                this.DisplayUI();

                this.ClearOutOfBoundsObjects();

                this.GameOverLogic();

                ScreenBuffer.Display();
            }
        }

        private void BulletLogic()
        {
            foreach (Bullet bullet in this.bullets)
            {
                // Move each bullet
                bullet.Move();

                // Check bullets for collision with ship
                if (this.ship.HasCollision(bullet))
                {
                    this.ship.Hit(bullet);
                }
            }
        }

        private void ClearActiveEffects()
        {
            foreach (Effect effect in this.ship.ElapsedEffects(Timer.ElapsedMilliseconds))
            {
                this.ship.RemoveEffect(effect);
            }
        }

        private void ClearBullets()
        {
            this.bullets.RemoveAll(bullet =>
                                   (bullet.Direction == Direction.Up && bullet.Position.Y < 0) ||
                                   (bullet.Direction == Direction.Down && bullet.Position.Y >= Game.Height));
        }

        private void ClearEffects()
        {
            // Remove effects where their position is out of the game
            this.effects.RemoveAll(effect =>
                                   effect.Position.Y > Game.Height);
        }

        private void ClearEnemies()
        {
            // Remove enemies that have no health or their position is out of the game
            this.enemies.RemoveAll(enemy =>
                                   enemy.Health <= 0 || enemy.Position.Y > Height);
        }

        private void ClearOutOfBoundsObjects()
        {
            // Clear bullets
            this.ClearBullets();

            // Clear enemies
            this.ClearEnemies();

            // Clear effects
            this.ClearEffects();

            // Clear ships active effects
            this.ClearActiveEffects();
        }

        private void DisplayUI()
        {
            // Display BorderLines
            for (int i = 0; i < Game.Height; i++)
            {
                ScreenBuffer.Write("|", playfield, i, ConsoleColor.White);
            }

            // Display health
            ConsoleColor healthColor;

            if (this.ship.IsInvulnerable)
            {
                healthColor = ConsoleColor.Magenta;
            }
            else
            {
                healthColor = ConsoleColor.Red;
            }

            ScreenBuffer.Write("Health: " + this.ship.Health, playfield + 3, 3, healthColor);

            // Display number of active weapons
            ScreenBuffer.Write("Weapons: " + this.ship.ActiveWeaponsCount(), playfield + 3, 6, ConsoleColor.White);

            // Display score
            ScreenBuffer.Write("Score: " + this.score, playfield + 3, 9, ConsoleColor.White);
        }

        private void EffectLogic()
        {
            foreach (Effect effect in this.effects)
            {
                // Move each effect
                effect.Move();

                // Check effects for collision with ship
                if (effect.HasCollision(this.ship))
                {
                    // Add the effect to the ship
                    this.ship.AddEffect(effect.Clone());

                    effect.Clear();
                }
            }
        }

        private void EnemyLogic()
        {
            foreach (Enemy enemy in this.enemies)
            {
                // Move each enemy
                enemy.Move();

                // Check enemies for collision with ship
                if (enemy.HasCollision(this.ship))
                {
                    // Kill the enemy and damage the ship
                    enemy.Hit(this.ship);
                    this.ship.Hit(enemy);
                }

                // Check enemy for collision with bullets
                foreach (Bullet bullet in this.bullets)
                {
                    // Check for collision
                    if (enemy.HasCollision(bullet))
                    {
                        this.score += enemy.Hit(bullet);
                    }
                }

                // Enemy fire
                enemy.Fire(this.bullets);
            }
        }

        private void GameLogic()
        {
            this.BulletLogic();

            this.EffectLogic();

            this.EnemyLogic();
        }

        private void GameOverLogic()
        {
            // Game over condition
            if (this.ship.Health <= 0)
            {
                Timer.Stop();
            }
        }

        private void GenerateEffects()
        {
            if (Timer.ElapsedMilliseconds >= this.lastEffectSpawn + (1000 / this.effectSpawnSpeed))
            {
                EffectType randomEffectType = (EffectType)this.effectTypes.GetValue(Random.Next(this.effectTypes.Length));

                // If we have max weapons we do not need add weapon effect
                if (randomEffectType == EffectType.AddWeapon && this.ship.ActiveWeaponsCount() == 3)
                {
                    while (randomEffectType == EffectType.AddWeapon)
                    {
                        randomEffectType = (EffectType)this.effectTypes.GetValue(Random.Next(this.effectTypes.Length));
                    }
                }

                Coord position = new Coord(Random.Next(0, playfield - 4), 0);

                this.effects.Add(new Effect(position, randomEffectType));

                this.lastEffectSpawn = Timer.ElapsedMilliseconds;
            }
        }

        private void GenerateEnemies()
        {
            if (Timer.ElapsedMilliseconds >= this.lastEnemySpawn + (1000 / this.enemySpawnSpeed))
            {
                EnemySize randomEnemySize = (EnemySize)this.enemySizes.GetValue(Random.Next(this.enemySizes.Length));

                int x = 0;

                if (randomEnemySize == EnemySize.Small)
                {
                    x = Random.Next(0, playfield - 2);
                }
                else if (randomEnemySize == EnemySize.Medium)
                {
                    x = Random.Next(0, playfield - 4);
                }
                else if (randomEnemySize == EnemySize.Large)
                {
                    x = Random.Next(0, playfield - 6);
                }

                this.enemies.Add(new Enemy(new Coord(x, 0), randomEnemySize));

                this.lastEnemySpawn = Timer.ElapsedMilliseconds;

                // Slightly increase enemy spawn speed
                this.enemySpawnSpeed += 0.015;
            }
        }

        private void GetUserInput()
        {
            // Get keys from player
            if (Console.KeyAvailable)
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                while (Console.KeyAvailable)
                {
                    key = Console.ReadKey(true);
                }

                ConsoleKey[] input = { ConsoleKey.UpArrow, ConsoleKey.UpArrow, ConsoleKey.DownArrow, ConsoleKey.DownArrow, ConsoleKey.LeftArrow, ConsoleKey.RightArrow, ConsoleKey.LeftArrow, ConsoleKey.RightArrow, ConsoleKey.B, ConsoleKey.A };
                if (key.Key == input[this.counter])
                {
                    this.counter++;
                }
                else
                {
                    this.counter = 0;
                }

                if (this.counter == 10)
                {
                    this.counter = 0;
                    this.ship.ActivateWeapon();
                    this.ship.ActivateWeapon();

                    // AddEffect();
                }

                switch (key.Key)
                {
                case ConsoleKey.LeftArrow:
                    // Move ship to the left
                    this.ship.MoveLeft(0);
                    break;
                case ConsoleKey.RightArrow:
                    // Move ship to the right
                    this.ship.MoveRight(playfield);
                    break;
                case ConsoleKey.Spacebar:
                    // Fire
                    this.ship.Fire(this.bullets);
                    break;
                case ConsoleKey.D1:
                    // Increase ship speed
                    this.ship.IncreaseSpeed();
                    break;
                case ConsoleKey.D2:
                    // Decrease ship speed
                    this.ship.DecreaseSpeed();
                    break;
                case ConsoleKey.D3:
                    // Activate weapon
                    this.ship.ActivateWeapon();
                    break;
                case ConsoleKey.D4:
                    // Deactivate weapon
                    this.ship.DeactivateWeapon();
                    break;
                }
            }
        }

        private void PrintObjects()
        {
            foreach (Bullet bullet in this.bullets)
            {
                // Print each bullets
                bullet.Print();
            }

            foreach (Effect effect in this.effects)
            {
                // Print each effect
                effect.Print();
            }

            foreach (Enemy enemy in this.enemies)
            {
                // Print each enemies
                enemy.Print();
            }

            // Print ship
            this.ship.Print();
        }
    }
}