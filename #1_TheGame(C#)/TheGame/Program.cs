﻿namespace TheGame
{
    using System;

    public class Program
    {
        public static void Main()
        {
            Game game = RunGame();

            DisplayScore(game);

            RunLeaderboard(game);
        }

        private static void RunLeaderboard(Game game)
        {
            LeaderBoard leaderBoard = new LeaderBoard();

            leaderBoard.WriteScore(game.Score);

            leaderBoard.Display();
        }

        private static void DisplayScore(Game game)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Game over! Your score is {0}", game.Score);
        }

        private static Game RunGame()
        {
            Game game = new Game();
            game.Run();
            return game;
        }
    }
}