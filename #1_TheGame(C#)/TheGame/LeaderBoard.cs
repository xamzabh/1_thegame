﻿namespace TheGame
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;

    [Serializable]
    public struct Score
    {
        public string Name;
        public long Number;
    }

    public class LeaderBoard
    {
        private string fileName = "scores.dat";
        private List<Score> scores = new List<Score>();

        public LeaderBoard()
        {
            this.ReadScores();
        }

        public void Display()
        {
            // Order scores descending by each score
            this.scores = this.scores.OrderByDescending(o => o.Number).ToList();

            int windowHeight = Game.Height;
            int windowWidth = Game.Width;

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Blue;

            // Leaderboard header
            Console.Write(new string('-', windowWidth));
            string leaderBoardHeader = "LEADERBOARD";
            Console.SetCursorPosition((windowWidth - leaderBoardHeader.Length) / 2, 1);
            Console.WriteLine(leaderBoardHeader);
            Console.Write(new string('-', windowWidth));

            // Display Scores
            for (int i = 0; i < this.scores.Count; i++)
            {
                if (i >= (windowHeight - 4) / 2)
                {
                    break;
                }

                switch (i)
                {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case 7:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
                case 8:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
                case 9:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
                }

                Console.WriteLine("{0}{1}{2}", ("#" + (i + 1)).PadRight(4), this.scores[i].Number.ToString().PadRight(10), this.scores[i].Name);
                Console.WriteLine();
            }
        }

        public void WriteScore(int score)
        {
            // Write new score
            Console.Write("Tell me your name: ");
            string name = Console.ReadLine();

            // Create new score
            this.scores.Add(new Score { Number = score, Name = name });

            // Open the file and write the scores list in in.
            using (var file = File.Open(this.fileName, FileMode.OpenOrCreate, FileAccess.Write))
            {
                var writer = new BinaryFormatter();
                writer.Serialize(file, this.scores); // Writes the entire list.
            }
        }

        private void ReadScores()
        {
            // Read scores
            using (var file = File.Open(this.fileName, FileMode.OpenOrCreate, FileAccess.Read))
            {
                var reader = new BinaryFormatter();

                if (new FileInfo(this.fileName).Length != 0)
                {
                    this.scores = (List<Score>)reader.Deserialize(file); // Reads the entire list.
                }
            }
        }
    }
}