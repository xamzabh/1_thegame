﻿namespace TheGame
{
    public class Coord
    {
        public Coord()
        {
            this.X = 0;
            this.Y = 0;
        }

        public Coord(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X { get; private set; }

        public int Y { get; private set; }

        public static Coord operator +(Coord a, Coord b)
        {
            return new Coord(a.X + b.X, a.Y + b.Y);
        }
    }
}